from django.http import JsonResponse

from .models import Presentation


def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    presentation_models = Presentation.objects.filter(conference=conference_id)

    presentations = [
        {
            "title": p.title,
            "status": p.status.name,
            "href": p.get_api_url(),
        }
        for p in presentation_models
    ]
    return JsonResponse({"presentations": presentations})


def api_show_presentation(request, id):
    """
    Returns the details for the Presentation model specified
    by the id parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    presentation_model = Presentation.objects.get(id=id)

    presentations = [
        {
            "presenter_name": presentation_model.presenter_name,
            "company_name": presentation_model.company_name,
            "presenter_email": presentation_model.presenter_email,
            "title": presentation_model.title,
            "synopsis": presentation_model.synopsis,
            "created": presentation_model.created,
            "status": presentation_model.status,
            "conference":  {
                "name": presentation_model.conference.name,
                "href": presentation_model.conference.get_api_url()
            }
        }]

    return JsonResponse({"presentations": presentations})
