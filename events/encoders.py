from common.json import ModelEncoder
from .models import Conference, Location


class LocationEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = ["name", "city", "room_count", "created", "updated"]

    def get_extra_data(self, o):  #o is the model that is currently being processed
        return {"state": o.state.abbreviation}


class ConferenceEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = ["name", "starts", "location", "ends", "description", "created", "updated", "max_presentations", "max_attendees"]
    encoders = {"location": LocationEncoder()}
